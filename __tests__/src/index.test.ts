import { SendEventOptions } from '../../src/types';

describe('Testing Publisher Kit', () => {
  const eventData: SendEventOptions = {
    channels: ['gdsdf'],
    event: 'anystring',
    payload: {
      anydata: '123',
    },
  };

  const mockResponse = (success: boolean) => {
    jest.doMock('request-promise', () => {
      if (success) {
        return jest.fn().mockResolvedValueOnce(true);
      }
      return jest.fn().mockRejectedValueOnce(false);
    });
  };

  const getNewPublisherKit = () => {
    const PublisherKit = require('../../src').default;
    return new PublisherKit();
  };

  beforeEach(() => {
    jest.resetModules();
  });
  it('should not allow sending event if it is not initialized', async () => {
    const publisherKitInstance = getNewPublisherKit();
    await expect(publisherKitInstance.sendEvent(eventData)).rejects.toThrow();
  });

  it('should allow sending event if library is already initialized', async () => {
    mockResponse(true);
    const publisherKitInstance = getNewPublisherKit();
    publisherKitInstance.init({
      serverApiKey: 'afisdf',
      appId: 'fjsks',
    });
    await expect(publisherKitInstance.sendEvent(eventData)).resolves.toBe(undefined);
  });

  it('should throw error when sending event API call fails', async () => {
    mockResponse(false);
    const publisherKitInstance = getNewPublisherKit();
    publisherKitInstance.init({
      serverApiKey: 'afisdf',
      appId: 'fjsks',
    });
    await expect(publisherKitInstance.sendEvent(eventData)).rejects.toThrow();
  });

  it('should not allow calling join user to channels API if library is not initialized', async () => {
    const publisherKitInstance = getNewPublisherKit();
    await expect(
      publisherKitInstance.joinUsersToChannels(['gdsdf'], ['fasdf', 'vxcvs']),
    ).rejects.toThrow();
  });

  it('should not allow calling remove user from channels API if library is not initialized', async () => {
    const publisherKitInstance = getNewPublisherKit();
    await expect(
      publisherKitInstance.removeUsersFromChannels(['gdsdf'], ['fasdf', 'vxcvs']),
    ).rejects.toThrow();
  });

  it('should allow calling join user if library is already initialized', async () => {
    mockResponse(true);
    const publisherKitInstance = getNewPublisherKit();
    publisherKitInstance.init({
      serverApiKey: 'afisdf',
      appId: 'fjsks',
    });
    await expect(
      publisherKitInstance.joinUsersToChannels(['gdsdf'], ['fasdf', 'vxcvs']),
    ).resolves.toBe(undefined);
  });

  it('should throw error when joining user API call fails', async () => {
    mockResponse(false);
    const publisherKitInstance = getNewPublisherKit();
    publisherKitInstance.init({
      serverApiKey: 'afisdf',
      appId: 'fjsks',
    });
    await expect(
      publisherKitInstance.joinUsersToChannels(['gdsdf'], ['fasdf', 'vxcvs']),
    ).rejects.toThrow();
  });

  it('should allow calling remove user if library is already initialized', async () => {
    mockResponse(true);
    const publisherKitInstance = getNewPublisherKit();
    publisherKitInstance.init({
      serverApiKey: 'afisdf',
      appId: 'fjsks',
    });
    await expect(
      publisherKitInstance.removeUsersFromChannels(['gdsdf'], ['fasdf', 'vxcvs']),
    ).resolves.toBe(undefined);
  });

  it('should throw error when removing user API call fails', async () => {
    mockResponse(false);
    const publisherKitInstance = getNewPublisherKit();
    publisherKitInstance.init({
      serverApiKey: 'afisdf',
      appId: 'fjsks',
    });
    await expect(
      publisherKitInstance.removeUsersFromChannels(['gdsdf'], ['fasdf', 'vxcvs']),
    ).rejects.toThrow();
  });

  it(`should not allow calling delete user's sessions API if library is not initialized`, async () => {
    const publisherKitInstance = getNewPublisherKit();
    await expect(publisherKitInstance.deleteUserSessions('gdsdf')).rejects.toThrow();
  });

  it(`should allow calling delete user's sessions if library is already initialized`, async () => {
    mockResponse(true);
    const publisherKitInstance = getNewPublisherKit();
    publisherKitInstance.init({
      serverApiKey: 'afisdf',
      appId: 'fjsks',
    });
    await expect(publisherKitInstance.deleteUserSessions('gdsdf')).resolves.toBe(undefined);
  });

  it(`should throw error when deleting user's sessions API call fails`, async () => {
    mockResponse(false);
    const publisherKitInstance = getNewPublisherKit();
    publisherKitInstance.init({
      serverApiKey: 'afisdf',
      appId: 'fjsks',
    });
    await expect(publisherKitInstance.deleteUserSessions('gdsdf')).rejects.toThrow();
  });

  it(`should not allow calling getUserChannels API if library is not initialized`, async () => {
    const publisherKitInstance = getNewPublisherKit();
    await expect(publisherKitInstance.getUserChannels('gdsdf')).rejects.toThrow();
  });

  it(`should allow calling getUserChannels if library is already initialized`, async () => {
    mockResponse(true);
    const publisherKitInstance = getNewPublisherKit();
    publisherKitInstance.init({
      serverApiKey: 'afisdf',
      appId: 'fjsks',
    });
    await expect(publisherKitInstance.getUserChannels('gdsdf')).resolves.toBe(true);
  });

  it(`should throw error when getUserChannels API call fails`, async () => {
    mockResponse(false);
    const publisherKitInstance = getNewPublisherKit();
    publisherKitInstance.init({
      serverApiKey: 'afisdf',
      appId: 'fjsks',
    });
    await expect(publisherKitInstance.getUserChannels('gdsdf')).rejects.toThrow();
  });

  it(`should not allow calling getAllConnections API if library is not initialized`, async () => {
    const publisherKitInstance = getNewPublisherKit();
    await expect(publisherKitInstance.getAllConnections()).rejects.toThrow();
  });

  it(`should allow calling getAllConnections if library is already initialized`, async () => {
    mockResponse(true);
    const publisherKitInstance = getNewPublisherKit();
    publisherKitInstance.init({
      serverApiKey: 'afisdf',
      appId: 'fjsks',
    });
    await expect(publisherKitInstance.getAllConnections()).resolves.toBe(true);
  });

  it(`should throw error when getAllConnections API call fails`, async () => {
    mockResponse(false);
    const publisherKitInstance = getNewPublisherKit();
    publisherKitInstance.init({
      serverApiKey: 'afisdf',
      appId: 'fjsks',
    });
    await expect(publisherKitInstance.getAllConnections()).rejects.toThrow();
  });

  it(`should not allow calling getChannelConnections API if library is not initialized`, async () => {
    const publisherKitInstance = getNewPublisherKit();
    await expect(publisherKitInstance.getChannelConnections('asdaf')).rejects.toThrow();
  });

  it(`should allow calling getChannelConnections if library is already initialized`, async () => {
    mockResponse(true);
    const publisherKitInstance = getNewPublisherKit();
    publisherKitInstance.init({
      serverApiKey: 'afisdf',
      appId: 'fjsks',
    });
    await expect(publisherKitInstance.getChannelConnections('asdaf')).resolves.toBe(true);
  });

  it(`should throw error when getChannelConnections API call fails`, async () => {
    mockResponse(false);
    const publisherKitInstance = getNewPublisherKit();
    publisherKitInstance.init({
      serverApiKey: 'afisdf',
      appId: 'fjsks',
    });
    await expect(publisherKitInstance.getChannelConnections('asdaf')).rejects.toThrow();
  });
});
