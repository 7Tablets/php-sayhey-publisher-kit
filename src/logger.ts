import 'winston-daily-rotate-file';

import path from 'path';
import { createLogger, format, transports } from 'winston';

const appLabel = `publisher-kit`;
const { combine, colorize, timestamp, label, printf } = format;

/* TODO : winston-daily-rotate-file
 */

const customFormat = ({ stringify = false } = {}) =>
  printf((info: any) => {
    const preMessage = `${info.timestamp} ${info.label} ${info.level}:`;
    const { message } = info;

    if (info.object) {
      if (stringify) {
        return `${preMessage} \n${JSON.stringify(message, null, 2)}`;
      }
      return `${preMessage} \n${message}`;
    }

    return `${preMessage} ${message}`;
  });

const enumerateErrorFormat = format((info: any) => {
  if (info.message instanceof Error) {
    info.message = {
      message: info.message.stack,
      ...info.message,
    };
    info.messageError = true;
  }

  if (info instanceof Error) {
    (info as any).messageError = true;
    return {
      message: info.stack,
      ...info,
    };
  }

  if (info.message instanceof Object) {
    info.object = true;
  }

  return info;
});

export const initLogger = (level: string, enable: boolean) =>
  createLogger({
    level,
    silent: !enable,
    format: enumerateErrorFormat(),
    transports: [
      new transports.DailyRotateFile({
        filename: 'application-%DATE%.log',
        datePattern: 'YYYY-MM-DD',
        zippedArchive: false, // keep false, this allows older files to be deleted
        maxSize: '20m',
        maxFiles: '15',
        dirname: path.resolve(__dirname, '..', 'logs', 'publisher-kit'),
        // NOTE: Do not use colorize file transport to avoid special characters
        format: combine(label({ label: appLabel }), timestamp(), customFormat({ stringify: true })),
      }),
      new transports.Console({
        format: combine(
          label({ label: appLabel }),
          timestamp(),
          colorize({ all: true }),
          // NOTE: Do not stringify objects as colorize does it
          customFormat({ stringify: false }),
        ),
      }),
    ],
  });
